package com.configurator.creator.pdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.configurator.offer.Offer;
import com.configurator.offer.OfferOption;
import com.configurator.offer.Summary;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class MainPdfCreator {

	private static Logger logger = LogManager.getLogger(MainPdfCreator.class);

	private static final float tableTotalWidth = 500f;

	/**
	 * 
	 * @param offer
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static ByteArrayInputStream create(Offer offer, String pdfName) throws FileNotFoundException, IOException {

		Document document = null;
		ByteArrayOutputStream documentAsByteArray = null;

		try {

			document = new Document();
			documentAsByteArray = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, documentAsByteArray);

			document.open();

			PdfPTable headerTable = null;

			headerTable = addTableHeader(offer.getSummary().isContract());
			document.add(headerTable);

			if (offer.getSummary().isContract()) {
				headerTable = addTableSecondHeader();
				document.add(headerTable);
			}

			PdfPTable separator = addSeparator("", false);
			document.add(separator);
			document.add(separator);

			PdfPTable infoTable = addTableInfo(offer.getSummary());
			document.add(infoTable);

			// add engine section
			addSection(document, "Motorisierung", offer.getEngine());

			// package
			addSection(document, "Pakete", offer.getOfferPackage());

			// dye
			addSection(document, "Lackierung", offer.getDye());

			// fabrics
			addSection(document, "Möbel", offer.getFabrics());

			// furniture
			addSection(document, "Polster", offer.getFurniture());

			// optionChassis
			addSection(document, "Optionen Chassis", offer.getOptionChassis());

			// optionConstruction
			addSection(document, "Optionen Aufbau", offer.getOptionConstruction());

			// optionExtra
			addSection(document, "Extra", offer.getOptionExtra());

			// add summary
			separator = addSeparator("", true);
			setupTable(separator);
			document.add(separator);
			PdfPTable summaryTable = addTableSummary(offer.getSummary());
			document.add(summaryTable);

			// add invoice footer
			if (offer.getSummary().isContract()) {
				// separator = addSeparator("", false);
				document.add(separator);
				document.add(separator);
				PdfPTable footerTable = addTableFooter();
				document.add(footerTable);
			}

			document.close();
		} catch (Exception e) {

			logger.error(e);

			if (null != document) {
				document.close();
			}

			return null;
		}

		return new ByteArrayInputStream(documentAsByteArray.toByteArray());
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 * @throws BadElementException
	 */
	private static PdfPTable addTableHeader(boolean isContract)
			throws BadElementException, MalformedURLException, URISyntaxException, IOException {

		PdfPTable table = new PdfPTable(new float[] { 0.7f, 1.55f, 0.75f, 1.2f });
		setupTable(table);

		Image modelImg = getImage("../assets/img/logo_pdf.png");

		float fontSize = 8f;

		// creating left top picture table
		PdfPTable pictureTable = new PdfPTable(1);

		PdfPCell imageCell = null;
		if (null != modelImg) {
			modelImg.scalePercent(80);
			imageCell = new PdfPCell(modelImg);
		} else {
			imageCell = new PdfPCell();
		}

		imageCell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		imageCell.setVerticalAlignment(Element.ALIGN_CENTER);
		pictureTable.addCell(imageCell);

		PdfPCell nesthousing = new PdfPCell(pictureTable);
		nesthousing.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		nesthousing.setPadding(0f);
		nesthousing.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// nesthousing.setColspan(2);

		table.addCell(nesthousing);

		PdfPCell cell = null;

		// creating Verbindliche Bestellung section
		PdfPTable headerTableMidleText = new PdfPTable(1);
		if (isContract) {
			cell = new PdfPCell(
					new Phrase("Verbindliche", new Font(FontFamily.HELVETICA, 20, Font.BOLD, BaseColor.BLACK)));
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headerTableMidleText.addCell(cell);

			cell = new PdfPCell(
					new Phrase("Bestellung", new Font(FontFamily.HELVETICA, 20, Font.BOLD, BaseColor.BLACK)));
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headerTableMidleText.addCell(cell);
		}
		PdfPCell nesthousingHeaderTable = new PdfPCell(headerTableMidleText);
		nesthousingHeaderTable.setPadding(0f);
		// nesthousingHeaderTable.setColspan(4);
		nesthousingHeaderTable.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		nesthousingHeaderTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nesthousingHeaderTable);

		// creating Verkäufer/Firma section
		cell = new PdfPCell(
				new Phrase("Verkäufer/Firma", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// headerTableMidleText.addCell(cell);
		table.addCell(cell);

		// creating header text
		PdfPTable headerTable = new PdfPTable(1);

		cell = new PdfPCell(
				new Phrase("Wohnmobilcenter", new Font(FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(
				new Phrase("- Sachsen GmbH -", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(new Phrase("Dresdener Str. 106a 02994 Bernsdorf",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(new Phrase("Tel.: 035723/ 92 82 1",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(new Phrase("wohnmobilcenter-sachsen@gmx.de",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(new Phrase("USt.ID-Nr. DE 268 66 3089",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		cell = new PdfPCell(
				new Phrase("Firmenstempel", new Font(FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.addCell(cell);

		nesthousingHeaderTable = new PdfPCell(headerTable);
		nesthousingHeaderTable.setPadding(0f);
		// nesthousingHeaderTable.setColspan(4);
		nesthousingHeaderTable.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		nesthousingHeaderTable.setHorizontalAlignment(Element.ALIGN_RIGHT);

		table.addCell(nesthousingHeaderTable);

		return table;
	}

	/**
	 * 
	 * @return
	 */
	private static PdfPTable addTableSecondHeader() {

		PdfPTable table = new PdfPTable(new float[] {1f, 0.8f, 1.2f });
		setupTable(table);

		float fontSize = 9;

		PdfPCell cell = new PdfPCell(
				new Phrase("Name, Firma:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.setColspan(2);
		table.addCell(cell);

		cell = new PdfPCell(
				new Phrase("Beruf/Gewerbe:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		table.addCell(cell);

		cell = new PdfPCell(
				new Phrase("geb.am:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("in:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("Ausweis-Nr./Ort.:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("Straße:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.setColspan(2);
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("Tel.", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("Wohnort:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.setColspan(2);
		table.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase("Fax:", new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.setColspan(2);
		table.addCell(cell);	

		return table;
	}

	/**
	 * 
	 * @param summary
	 * @return
	 */
	private static PdfPTable addTableInfo(Summary summary) {

		// PdfPTable table = new PdfPTable(new float[] { 1.3f, 0.1f, 1.6f, 1f });
		PdfPTable table = new PdfPTable(3);

		Image modelImg = getImage(summary.getModelImgPath());
		Image groundPlanImg = getImage(summary.getGroundPlanImgPath());

		// creating left top picture table
		PdfPTable pictureTable = new PdfPTable(1);

		PdfPCell imageCell = null;
		if (null != modelImg) {
			// modelImg.scalePercent(40);
			modelImg.scaleToFit(130f, 130f);
			imageCell = new PdfPCell(modelImg);
		} else {
			imageCell = new PdfPCell(new Phrase("No image"));
		}
		imageCell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		pictureTable.addCell(imageCell);

		if (null != groundPlanImg) {
			// groundPlanImg.scalePercent(25);
			groundPlanImg.scaleToFit(140f, 140f);
			imageCell = new PdfPCell(groundPlanImg);
		} else {
			imageCell = new PdfPCell(new Phrase("No image"));
		}
		imageCell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		pictureTable.addCell(imageCell);

		PdfPCell nesthousing = new PdfPCell(pictureTable);
		nesthousing.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		// nesthousing.setPadding(0f);

		table.addCell(nesthousing);

//		// add separator
//		Phrase phraseSeparator = new Phrase("   ",
//				new Font(FontFamily.HELVETICA, 20f, Font.BOLD, BaseColor.BLACK));
//		PdfPCell cellSeparator = new PdfPCell(phraseSeparator);
//		cellSeparator.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
//		table.addCell(cellSeparator);

		// creating summary text
		PdfPTable summaryTextTable = new PdfPTable(1);

		Phrase phrase = new Phrase(summary.getFullName(),
				new Font(FontFamily.HELVETICA, 20f, Font.BOLD, BaseColor.BLACK));
		PdfPCell cell = new PdfPCell(phrase);
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		summaryTextTable.addCell(cell);

		cell = new PdfPCell(new Phrase("   ", new Font(FontFamily.HELVETICA, 10f, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		summaryTextTable.addCell(cell);

		StringBuilder sb = new StringBuilder();
		sb.append(summary.getWeight());
		sb.append(" kg ");
		cell = new PdfPCell(new Phrase(String.valueOf(sb.toString()),
				new Font(FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		summaryTextTable.addCell(cell);

		sb = new StringBuilder();
		sb.append(summary.getDimY());
		sb.append("m x ");
		sb.append(summary.getDimZ());
		sb.append("m x ");
		sb.append(summary.getDimX());
		sb.append(" m");		
		cell = new PdfPCell(new Phrase(String.valueOf(sb.toString()),
				new Font(FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		summaryTextTable.addCell(cell);

		sb = new StringBuilder();
		sb.append(summary.getSleepingCapacity());
		sb.append(" Schlafplätze");
		cell = new PdfPCell(new Phrase(String.valueOf(sb.toString()),
				new Font(FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		summaryTextTable.addCell(cell);

		sb = new StringBuilder();
		sb.append(summary.getSeatingCapacity());
		sb.append(" Sitzplätze");
		cell = new PdfPCell(new Phrase(String.valueOf(sb.toString()),
				new Font(FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		summaryTextTable.addCell(cell);

		PdfPCell nesthousingsummaryTextTable = new PdfPCell(summaryTextTable);
		nesthousingsummaryTextTable.setPadding(0f);
		nesthousingsummaryTextTable.setColspan(2);
		nesthousingsummaryTextTable
				.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);

		table.addCell(nesthousingsummaryTextTable);

		return table;
	}

	/**
	 * 
	 * @param summary
	 * @return
	 */
	private static PdfPTable addTableSummary(Summary summary) {

		PdfPTable summaryTextTable = new PdfPTable(new float[] { 4.9f, 1f });
		setupTable(summaryTextTable);

		PdfPCell cell = addTableSummaryCell("Total: ");
		summaryTextTable.addCell(cell);

		StringBuilder sb = new StringBuilder();
		sb.append(summary.getTotalPrice());
		sb.append(" €");
		cell = addTableSummaryCell(sb.toString());
		summaryTextTable.addCell(cell);

		if (0 < summary.getPromoPrice()) {
			cell = addTableSummaryCell("Rabat: ");
			summaryTextTable.addCell(cell);

			sb = new StringBuilder();
			sb.append("-");
			sb.append(summary.getPromoPrice());
			sb.append(" €");
			cell = addTableSummaryCell(sb.toString());
			summaryTextTable.addCell(cell);

			cell = addTableSummaryCell("Endpreis: ");
			summaryTextTable.addCell(cell);

			sb = new StringBuilder();
			sb.append(summary.getTotalPrice() - summary.getPromoPrice());
			sb.append(" €");
			cell = addTableSummaryCell(sb.toString());
			summaryTextTable.addCell(cell);
		}

		return summaryTextTable;
	}

	private static PdfPCell addTableSummaryCell(String value) {
		PdfPCell cell = new PdfPCell(new Phrase(value, new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		return cell;
	}

	/**
	 * 
	 * @param document
	 * @param sectionName
	 * @param offerPackage
	 * @throws DocumentException
	 */
	private static void addSection(Document document, String sectionName, List<OfferOption> offerPackage)
			throws DocumentException {
		PdfPTable separator = addSeparator(sectionName, true);
		setupTable(separator);
		document.add(separator);
		PdfPTable optionTable = getOptionTable(offerPackage);
		setupTable(optionTable);
		document.add(optionTable);
	}

	/**
	 * 
	 * @param table
	 */
	private static void setupTable(PdfPTable table) {
		table.setTotalWidth(tableTotalWidth);
		table.setLockedWidth(true);
	}

	/**
	 * 
	 * @param options
	 * @return
	 */
	private static PdfPTable getOptionTable(List<OfferOption> options) {
		PdfPTable table = new PdfPTable(10);

		if (null == options) {
			return table;
		}

		float fontSize = 7f;

		for (OfferOption oo : options) {

			PdfPCell cell = new PdfPCell(new Phrase(oo.getOptionName(),
					new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
			// PdfPCell cell = new PdfPCell(new Phrase("1"));
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			cell.setColspan(6);
			table.addCell(cell);

			Image descriptionImg = null;
			descriptionImg = getImage(oo.getImagePath());

			if (oo.getImagePath() != null && oo.getImagePath().isEmpty() || null == descriptionImg) {
				cell = new PdfPCell(new Phrase(oo.getDescription(),
						new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
			} else {
				if (null != descriptionImg) {
					descriptionImg.scaleToFit(25f, 25f);
					cell = new PdfPCell(descriptionImg);
				}
			}
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			cell.setColspan(3);
			table.addCell(cell);

			StringBuilder sb = new StringBuilder();
			sb.append(oo.getPriceInclVat());
			sb.append(" €");
			cell = new PdfPCell(
					new Phrase(sb.toString(), new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cell);
		}

		return table;
	}

	/**
	 * 
	 * @return
	 */
	private static PdfPTable addTableFooter() {
		PdfPTable table = new PdfPTable(2);
		setupTable(table);

		float fontSize = 10f;

		String text = "Diese Bestellung ist bis zu 14 Tagen bindet. Der Kaufvertrag ist abgeschlossen, wenn der Verkäufer die Annahme der Bestellung des naher bezeichneten Kaufgegenstandes innerhalb der Frist schriftlich bestätigt oder die Lieferung aufführt.\n"
				+ "Eine Anzahlung von 5000,00 € wird nach Erhalt der Auftragsbestätigung fällig, außer bei Vollfinanzierung.";

		PdfPCell cell = new PdfPCell(
				new Phrase(text, new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setColspan(2);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 60, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setColspan(2);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("___________________________________",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("___________________________________",
				new Font(FontFamily.HELVETICA, fontSize, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		cell = new PdfPCell(
				new Phrase("Ort / Datum ", new Font(FontFamily.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		cell = new PdfPCell(
				new Phrase("Unterschrift Käufer", new Font(FontFamily.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		return table;
	}

	/**
	 * 
	 * @param basePath
	 * @return
	 * @throws URISyntaxException
	 * @throws BadElementException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private static Image getImage(String basePath) {

		Image img = null;
		try {
			String fullPath = "./";
			fullPath += basePath;

			// Path path = Paths.get(ClassLoader.getSystemResource(fullPath).toURI());
			Path path = Paths.get(fullPath);

			img = Image.getInstance(path.toAbsolutePath().toString());
		} catch (BadElementException | IOException e) {
			logger.error(e);
			img = null;
		}

		return img;
	}

	/**
	 * 
	 * @param sectionName
	 * @param table
	 */
	private static PdfPTable addSeparator(String sectionName, boolean separatorLine) {
		PdfPTable table = new PdfPTable(1);

		PdfPCell cell = new PdfPCell(new Phrase("  ", new Font(FontFamily.HELVETICA, 20f, Font.BOLD, BaseColor.BLACK)));
		if (separatorLine) {
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
		} else {
			cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		}
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(sectionName, new Font(FontFamily.HELVETICA, 12f, Font.ITALIC, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("  ", new Font(FontFamily.HELVETICA, 1f, Font.BOLD, BaseColor.BLACK)));
		cell.disableBorderSide(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
		table.addCell(cell);

		return table;
	}
}
