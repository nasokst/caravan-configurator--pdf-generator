package com.configurator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.Banner;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = "com.configurator")
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@EnableAsync
public class PdfManagerService {

	private static final Logger logger = LogManager.getLogger(PdfManagerService.class);

	public static void main(String[] argv) {

		try {
			SpringApplication app = new SpringApplication(PdfManagerService.class);
			app.setBannerMode(Banner.Mode.OFF);
			app.run(argv);
		} catch (Exception e) {
			logger.error("Unexpected error on application startup!", e);

		}
	}
}
