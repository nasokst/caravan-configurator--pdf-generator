package com.configurator.offer.impl;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.configurator.offer.IOfferController;
import com.configurator.offer.Offer;
import com.configurator.offer.ResponseBean;
import com.configurator.offer.service.GeneratePdfOfferService;

@RestController
public class OfferController implements IOfferController {

	@Autowired
	private GeneratePdfOfferService generatePdfOfferService;

	@Override
	public ResponseEntity<?> generateAndStorePdfOffer(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Offer offer) {

		String brand = request.getHeader("accept-brand");
		String language = request.getHeader("accept-language");

		ResponseBean responseBean = generatePdfOfferService.process_service(offer, brand, language);

		if (null == responseBean.getPayload()) {
			return new ResponseEntity<Object>("Error", responseBean.getStatus());
		}

		String fileName = (String) ((List<Object>) responseBean.getPayload()).get(0);
		ByteArrayInputStream pdfFileByteArray = (ByteArrayInputStream) ((List<Object>) responseBean.getPayload())
				.get(1);

		response.addHeader("Content-Disposition", "inline; filename=" + fileName);
		response.addHeader("Content-file-name", fileName);

		return new ResponseEntity<Object>(new InputStreamResource(pdfFileByteArray), responseBean.getStatus());
	}

}
