package com.configurator.offer;

public class Summary {
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getPromoPrice() {
		return promoPrice;
	}

	public void setPromoPrice(double promoPrice) {
		this.promoPrice = promoPrice;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getDimX() {
		return dimX;
	}

	public void setDimX(double dimX) {
		this.dimX = dimX;
	}

	public double getDimY() {
		return dimY;
	}

	public void setDimY(double dimY) {
		this.dimY = dimY;
	}

	public double getDimZ() {
		return dimZ;
	}

	public void setDimZ(double dimZ) {
		this.dimZ = dimZ;
	}

	public long getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(long seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public long getSleepingCapacity() {
		return sleepingCapacity;
	}

	public void setSleepingCapacity(long sleepingCapacity) {
		this.sleepingCapacity = sleepingCapacity;
	}

	public String getModelImgPath() {
		return modelImgPath;
	}

	public void setModelImgPath(String modelImgPath) {
		this.modelImgPath = modelImgPath;
	}

	public String getGroundPlanImgPath() {
		return groundPlanImgPath;
	}

	public void setGroundPlanImgPath(String groundPlanImgPath) {
		this.groundPlanImgPath = groundPlanImgPath;
	}

	public boolean isContract() {
		return contract;
	}

	public void setContract(boolean contract) {
		this.contract = contract;
	}

	private String fullName;
	private double totalPrice;
	private double promoPrice;
	private double weight;
	private double dimX;
	private double dimY;
	private double dimZ;
	private long seatingCapacity;
	private long sleepingCapacity;
	private String modelImgPath;
	private String groundPlanImgPath;
	private boolean contract;
}
