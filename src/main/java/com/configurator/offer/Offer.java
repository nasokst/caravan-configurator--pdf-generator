package com.configurator.offer;

import java.util.List;

public class Offer {

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Summary getSummary() {
		return summary;
	}
	public void setSummary(Summary summary) {
		this.summary = summary;
	}
	public List<OfferOption> getEngine() {
		return engine;
	}
	public void setEngine(List<OfferOption> engine) {
		this.engine = engine;
	}
	public List<OfferOption> getOfferPackage() {
		return offerPackage;
	}
	public void setOfferPackage(List<OfferOption> offerPackage) {
		this.offerPackage = offerPackage;
	}
	public List<OfferOption> getDye() {
		return dye;
	}
	public void setDye(List<OfferOption> dye) {
		this.dye = dye;
	}
	public List<OfferOption> getFabrics() {
		return fabrics;
	}
	public void setFabrics(List<OfferOption> fabrics) {
		this.fabrics = fabrics;
	}
	public List<OfferOption> getFurniture() {
		return furniture;
	}
	public void setFurniture(List<OfferOption> furniture) {
		this.furniture = furniture;
	}
	public List<OfferOption> getOptionChassis() {
		return optionChassis;
	}
	public void setOptionChassis(List<OfferOption> optionChassis) {
		this.optionChassis = optionChassis;
	}
	public List<OfferOption> getOptionConstruction() {
		return optionConstruction;
	}
	public void setOptionConstruction(List<OfferOption> optionConstruction) {
		this.optionConstruction = optionConstruction;
	}
	public List<OfferOption> getOptionExtra() {
		return optionExtra;
	}
	public void setOptionExtra(List<OfferOption> optionExtra) {
		this.optionExtra = optionExtra;
	}
	public byte[] getOfferPdf() {
		return offerPdf;
	}
	public void setOfferPdf(byte[] offerPdf) {
		this.offerPdf = offerPdf;
	}


	private long id;
	private Client client;
	private Summary summary;
	private List<OfferOption> engine;
	private List<OfferOption> offerPackage;
	private List<OfferOption> dye;
	private List<OfferOption> fabrics;
	private List<OfferOption> furniture;
	private List<OfferOption> optionChassis;
	private List<OfferOption> optionConstruction;
	private List<OfferOption> optionExtra;
	private byte[] offerPdf;
}
