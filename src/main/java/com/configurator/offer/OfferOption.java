package com.configurator.offer;

public class OfferOption {
	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public double getPriceInclVat() {
		return priceInclVat;
	}

	public void setPriceInclVat(double priceInclVat) {
		this.priceInclVat = priceInclVat;
	}

	private String optionName;
	private String description;
	private String imagePath;
	private double priceInclVat;
}
