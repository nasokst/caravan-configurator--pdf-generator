package com.configurator.offer.service;

import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.configurator.creator.pdf.MainPdfCreator;
import com.configurator.offer.Offer;
import com.configurator.offer.ResponseBean;

@Service("generatePdfOfferService")
public class GeneratePdfOfferService {

	public ResponseBean process_service(Object object, String brand, String language) {

		Offer offer = (Offer) object;

		StringBuilder fileNameBuilder = new StringBuilder();
		fileNameBuilder.append(offer.getSummary().getFullName());
		fileNameBuilder.append("-");
		fileNameBuilder.append(offer.getClient().getFirstName());
		fileNameBuilder.append("-");
//		fileNameBuilder.append(offer.getClient().getSecondName());
//		fileNameBuilder.append("-");
		fileNameBuilder.append(LocalDateTime.now());
		fileNameBuilder.append(".pdf");

		boolean pdfGenerationOK = false;
		
		ByteArrayInputStream pdfFileByteArray = null;
		try {
			pdfFileByteArray = MainPdfCreator.create(offer, fileNameBuilder.toString());
			pdfGenerationOK = null != pdfFileByteArray;
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!pdfGenerationOK) {
			return new ResponseBean(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		List<Object> result = new ArrayList<Object>();
		result.add(fileNameBuilder.toString());
		result.add(pdfFileByteArray);

		return new ResponseBean(result, HttpStatus.OK);
	}

}
