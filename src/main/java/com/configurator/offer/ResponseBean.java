package com.configurator.offer;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ResponseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7739614152913690025L;
	
	private HttpStatus status;

	private Object payload;
	
	public ResponseBean(Object payload, HttpStatus status) {
		this.payload = payload;
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

}
